#!/bin/bash

project_path=$( cd "$(dirname "${BASH_SOURCE[0]}")/.." ; pwd -P )

sudo setcap cap_net_raw+eip ${project_path}/target/debug/magic-network-audio
