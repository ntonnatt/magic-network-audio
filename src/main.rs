use clap::Parser;

use networkmanager::devices::{Any, Device};
use networkmanager::NetworkManager;

use dbus::blocking::Connection;

extern crate pnet;
use pnet::datalink::Channel::Ethernet;
use pnet::datalink::{self, NetworkInterface};
use pnet::packet::ethernet::{EthernetPacket, MutableEthernetPacket};
use pnet::packet::{FromPacket, MutablePacket, Packet};

use ctrlc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

#[derive(Parser)]
#[command(version, about, long_about = None, arg_required_else_help = true)]
struct Cli {
    interface_name: Option<String>,
    #[clap(long, short, action)]
    list: bool,
}

fn main() {
    let args = Cli::parse();

    let interfaces = datalink::interfaces();
    if args.list {
        for intf in interfaces {
            println!("- {:?}", intf.name);
        }
        return;
    }

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    let interface_name = match args.interface_name {
        None => return,
        Some(interface) => interface,
    };
    let interface_names_match = |iface: &NetworkInterface| iface.name == interface_name;
    println!("Network Interface: {:?}", interface_name);
    let interface = interfaces
        .into_iter()
        .filter(interface_names_match)
        .next()
        .unwrap();

    let dbus_connection = match Connection::new_system() {
        Ok(conn) => conn,
        Err(e) => panic!("Couldn't establish DBUS connection: {}", e),
    };

    let nm = NetworkManager::new(&dbus_connection);

    let nm_device = match nm.get_device_by_ip_iface(&interface_name) {
        Ok(dev) => match dev {
            Device::Ethernet(dev) => dev,
            _ => panic!("Not an Ethernet device!"),
        },
        Err(e) => panic!("Couldn't get device from NetworkManager: {:?}", e),
    };

    nm_device
        .set_managed(false)
        .expect("Failed to disable device management in NetworkManager");

    let (mut _tx, mut rx) = match datalink::channel(&interface, Default::default()) {
        Ok(Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("Unhandled channel type"),
        Err(e) => panic!("An error occured when creating the datalink channel: {}", e),
    };

    while running.load(Ordering::SeqCst) {
        match rx.next() {
            Ok(packet) => {
                if let Some(ethernet_packet) = EthernetPacket::new(packet) {
                    println!("New packet on {}", interface.name);
                    println!(
                        "{} => {}: {}",
                        ethernet_packet.get_destination(),
                        ethernet_packet.get_source(),
                        ethernet_packet.get_ethertype()
                    );
                    let packet = ethernet_packet.packet();
                    let payload = ethernet_packet.payload();
                    let from_packet = ethernet_packet.from_packet();
                    //println!("---");
                    println!("packet: {:?}", packet);
                    // print the full packet as an array of u8
                    println!("payload: {:?}", payload);
                    // print the payload as an array of u8
                    println!("from_packet: {:?}", from_packet);
                    // print the hearder infos: mac address, ethertype, ...
                    // and the payload as an array of u8
                    println!("---");
                }
            }
            Err(e) => {
                panic!("An error occured while reading: {}", e);
            }
        }
    }

    nm_device
        .set_managed(true)
        .expect("Couldn't reenable device management in NetworkManager");
}
